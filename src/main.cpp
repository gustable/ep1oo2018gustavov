#include <iostream>
#include <fstream>
#include <string>
#include "imagem.hpp"
#include "ppm.hpp"
#include "pgm.hpp"

using namespace std;

int main(int argc, char **argv) {

	Ppm* imagem1;
	imagem1 = new Ppm("lena.ppm");

	Ppm * imagem2;
	imagem2 = new Ppm("exemplo.ppm");

	Pgm * imagem3;
	imagem3 = new Pgm("exemplo.pgm");
	imagem3->descobre_msg();

	Pgm * imagem4;
	imagem3 = new Pgm("blocks.pgm");

	return 0;
}
