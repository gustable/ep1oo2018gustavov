#include "pgm.hpp"
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <bits/stdc++.h>

using namespace std;

Pgm::Pgm(string arquivo) : Imagem(arquivo){}

Pgm::~Pgm(){}

void Pgm::descobre_msg(){
	char alfabeto[] {'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'};
	char upper_alfabeto[] {'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'};

	string senha_str = getSenha();
	int senha_int;
	stringstream convert(senha_str);
	if (!(convert >> senha_int)) senha_int = 0;

	for (int i = 0; i < 26; ++i){
		alfabeto[i] -= senha_int;
		if(alfabeto[i] < 97) alfabeto[i] += 26;
		upper_alfabeto[i] -= senha_int;
		if(upper_alfabeto[i] < 65) upper_alfabeto[i] += 26;
	}

	//continuar a implementação
}