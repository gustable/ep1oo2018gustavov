#include "imagem.hpp"
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>

using namespace std;

Imagem::Imagem(string arquivo){
	this->arquivo = arquivo;
	ifstream myFile;
	myFile.open(this->arquivo, ios::binary);
    if (myFile.good()) {
    	cout << arquivo << endl;

		getline(myFile, numId);
		myFile.get(this->comentario);
		myFile >> inicio_msg;
		myFile >> tamanho_msg;
		myFile >> senha;
		myFile >> lar;
		myFile >> alt;
		myFile >> intensidade;

		myFile.clear();
		myFile.seekg(0, ios::beg);

		int conta_linha = 0;
		int tamanho_descricao = 0;
		char char_atual;

		while(conta_linha <= 3){
			myFile.get(char_atual);

			if(char_atual == '\n') {
				tamanho_descricao++;
				conta_linha++;
			} else {
				tamanho_descricao++;
			}
		} 

		pixel1 = tamanho_descricao + getInicio_msg();

		myFile.clear();
		myFile.seekg(getPixel1(), ios::beg);

		char msg[tamanho_msg];

		for (int i = 0; i < tamanho_msg; ++i){
			myFile.get(msg[i]);
		}

		string alloc_msg(msg);
		str_msg = alloc_msg;

		myFile.close();

    } else if(myFile.fail()){
    	cerr << "Não conseguimos abrir sua imagem" << endl;
    	myFile.close();
    } else {
    	cerr << "Erro desconhecido" << endl;
    }
}

Imagem::~Imagem(){}

int Imagem::getPixel1(){
	return pixel1;
}

int Imagem::getInicio_msg(){
	return inicio_msg;
}

string Imagem::getSenha(){
	return senha;
}
string Imagem::getStr_msg(){
	return str_msg;
}

void Imagem::descobre_msg(){}