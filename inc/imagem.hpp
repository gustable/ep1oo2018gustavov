#ifndef IMAGEM_HPP
#define IMAGEM_HPP
#include <string>

using namespace std;

class Imagem{
private:
	string numId, arquivo, senha, str_msg;
	int lar, alt, intensidade, inicio_msg, pixel1=0;
	char comentario;

protected:
	int tamanho_msg;

public:
	Imagem(string arquivo);
	~Imagem();

	int getPixel1();
	int getInicio_msg();
	string getSenha();
	string getStr_msg();

	virtual void descobre_msg();
};

#endif