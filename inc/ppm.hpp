#ifndef PPM_HPP
#define PPM_HPP
#include "imagem.hpp"
#include <string>

using namespace std;

class Ppm : public Imagem {
public:
	Ppm(string arquivo);
	~Ppm();

	void descobre_msg();
};

#endif