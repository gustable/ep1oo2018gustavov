#ifndef PGM_HPP
#define PGM_HPP
#include "imagem.hpp"
#include <string>

using namespace std;

class Pgm : public Imagem {
public:
	Pgm(string arquivo);
	~Pgm();

	void descobre_msg();
};

#endif